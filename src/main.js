import { createApp } from 'vue'
import './app.css'
import 'reset-css'
import '@/assets/iconfont/iconfont.css'
import App from './App.vue'
import {routerInitFn} from './router/routerInit'
import piniaInit from '@/state/piniaInit'

const app=createApp(App);
// 初始化router
routerInitFn(app);
// 初始化仓库
piniaInit(app);
app.mount('#app');

