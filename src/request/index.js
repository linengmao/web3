import axios from 'axios'

const request = axios.create({
    // baseURL: 'http://127.0.0.1:3001',
    baseURL: 'http://111.230.110.109:3001',
    timeout: 6000,
  });

// 添加请求拦截器
request.interceptors.request.use(function (config) {
  let token=localStorage.getItem('token');
  if(token){
    config.headers['token']=token;
  }
    return config;
  }, function (error) {
    return Promise.reject(error);
  });

// 添加响应拦截器
request.interceptors.response.use(function (response) {
    return response.data;
  }, function (error) {
    return Promise.reject(error);
  });

  export default request;