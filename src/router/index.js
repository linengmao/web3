import { createRouter, createWebHashHistory } from "vue-router";

const routes = [
  { path: "/", name: "index", component: () => import("@/views/community.vue") },
  { path: "/destination",name: "destination", component: () => import("@/views/destination.vue") },
  { path: "/travel", name: "travel", component: () => import("@/views/travel.vue") },
  { path: "/community", name: "community", component: () => import("@/views/home.vue") },
  { path: "/aboutus", name: "aboutus", component: () => import("@/components/aboutUs.vue") }
];
const router = createRouter({
  history: createWebHashHistory(),
  routes,
  strict: true,
});

export default router;
