import router from './index';

export function routerInitFn(app){
    return app.use(router)
}