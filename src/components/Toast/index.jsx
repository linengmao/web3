import { createApp } from "vue";
import './index.css'

const dom = {
  props: ["title", "ToastType", "icon"],
  render(h) {
    const { $props, $emit } = h;
    return <div className={['toast', $props.ToastType].join(' ')}>
              <i className={['iconfont', $props.icon].join(' ')}></i>
              <div class="text">{$props.title}</div>
          </div>
  },
};

export function Toast({
  title = "内容区域",
  date = 3000,
  ToastType = "loading",
  icon = "",
}) {
  const div = document.createElement("div");
  div.className = "toast-box";
  document.body.appendChild(div);
  // icon处理
  /**
   * 成功（success）
   * 失败（error）
   * 警告（warning）
   * 加载（loading）
   */
  const iconobj = {
    success: "icon-chenggong",
    error: "icon-shibai",
    warning: "icon-warning",
    loading: "icon-jiazai",
  };
  if (!icon) {
    icon = iconobj[ToastType];
  }
  // 渲染组件
  const app = createApp(dom, {
    title,
    ToastType,
    icon,
  });
  app.mount(div);
  // 启动定时器
  setTimeout(()=>{
    app.unmount()
    div.remove()
  },date)
}
