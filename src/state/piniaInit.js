import {createPinia} from 'pinia';
function piniaInit(app){
    const pinia=createPinia()
    app.use(pinia)
}

export default piniaInit;